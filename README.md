# CodeProbe  
*Application d'analyse statique de code*
---
### Lancer en ligne de commande : 

#### Installer : 
``mvn clean install``

#### Lancer le cli : 
``mvn exec:java "-Dexec.args=-cli"``

#### Lancer le gui : 
``mvn exec:java "-Dexec.args=-gui"``

### Lancer automatiquement : 

#### Sous windows : double cliquer sur le fichier ``run-cli.bat`` ou ``run-gui.bat``

#### Sous linux : lancer ``./run-cli.sh`` ou ``./run-gui.sh``

---

### Utilisation :

#### Sélection du projet : 
Par défaut le projet analysé est celui du répertoire courant (le code de l'application CodeProbe). 

#### Informations :

Que ce soit en interface GUI ou interface CLI, une partie des informations tirées de l'analyse sont affichées d'emblée :

- Nombre de classes de l'application 
- Nombres de LoC de l'application
- Nombres de methodes de l'application
- Nombres de packages de l'application
- Nombre moyen de méthodes par classe
- Nombre moyen de LoC par méthode
- Nombre moyen d'attributs par classe 

Puis des options sont proposées pour afficher d'autres informations :

- Top X % des classes qui possèdent le plus grand nombre de méthodes
- Top X % des classes qui possèdent le plus grand nombre d'attributs
- Classes qui sont dans les deux top X %
- Classes qui ont plus de X méthodes
- X % des méthodes avec le plus de lignes de code par classe

Ces options dépendent d'un paramètre flottant ou entier. 

Enfin deux dernières options sont proposées : 

- Changer de projet 
- Afficher le graphe d'appel

Le graphe d'appel affiche les invocations successives de méthodes en partant du main. 