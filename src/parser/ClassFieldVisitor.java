package parser;

import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.FieldDeclaration;
import org.eclipse.jdt.core.dom.TypeDeclaration;

public class ClassFieldVisitor extends ASTVisitor {
    private int fieldCount = 0;
    private int classCount = 0;

    public boolean visit(TypeDeclaration node) {
        if (!node.isInterface()) {
            classCount++;
        }
        return super.visit(node);
    }

    public boolean visit(FieldDeclaration node) {
        fieldCount++;
        return super.visit(node);
    }

    public float getFieldCount() {
        return fieldCount;
    }

    public float getClassCount() {
        return classCount;
    }
}
