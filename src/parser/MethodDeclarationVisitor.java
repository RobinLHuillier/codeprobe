package parser;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.MethodDeclaration;

import models.CallGraph;

public class MethodDeclarationVisitor extends ASTVisitor {
	List<MethodDeclaration> methods = new ArrayList<MethodDeclaration>();
	CallGraph callMap = new CallGraph();

	public boolean visit(MethodDeclaration node) {
		methods.add(node);
		
		MethodInvocationVisitor methodInvocationVisitor = new MethodInvocationVisitor();
        node.accept(methodInvocationVisitor);
        callMap.addMethodInvocations(node, methodInvocationVisitor.getMethods());
		
		return super.visit(node);
	}
	
	public List<MethodDeclaration> getMethods() {
		return methods;
	}
	
	public CallGraph getMethodsMap() {
		return callMap;
	}
	
	public int getMaxParameters() {
		 return methods.stream()
				 .mapToInt(method -> method.parameters().size())
				 .max().orElse(0);
	}
}
