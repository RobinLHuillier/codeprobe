package parser;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.CompilationUnit;

import exceptions.FolderInvalidException;
import models.CallGraph;
import models.MapMethodLoc;

public class JDTASTParser implements Parser {
	private String projectPath;
	private String srcPath = "/src";
	private String projectSourcePath = projectPath + srcPath;
	private String jrePath;
	private String[] encoding = new String[] { "UTF-8" };
	private boolean includeRunningVMBootclasspath = true;
	private ArrayList<CompilationUnit> units;
	
	public JDTASTParser(String projectPath, String jrePath) {
		setProjectPath(projectPath);
		setJrePath(jrePath);
	}
	
	public JDTASTParser() {}
	
	public void configure() {
		String osName = System.getProperty("os.name").toLowerCase();
		if (osName.contains("win")) {
			srcPath = "\\src";
		} else {	
			srcPath = "/src";
		}
		encoding = new String[] { "UTF-8" };
		includeRunningVMBootclasspath = true;
	}
	
	public void initialize() {
		createAllParsers();
	}
	
	public void setProjectPath(String path) {
		projectPath = path;
		projectSourcePath = projectPath + srcPath;
	}
	
	public void setJrePath(String path) {
		jrePath = path;
	}
	

	public String getProjectPath() {
		return projectPath;
	}
	
	public String getJrePath() {
		return jrePath;
	}
	
	public CallGraph getCallGraph() {
		MethodDeclarationVisitor visitor = new MethodDeclarationVisitor();
		visitAll(visitor);
		return visitor.getMethodsMap();
	}
	
	public float getAvgMethodByClassApp() {
		float meth = getNbMethodsApp();
		float clas = getNbClassesApp();
		
		return clas == 0 ? 0 : meth/clas;
	}
	
	public float getAvgLocByMethodApp() {
		int methodCount = 0;
		int locCount = 0;
		for (CompilationUnit unit: units) {
			MethodLocVisitor visitor = new MethodLocVisitor(unit);
			unit.accept(visitor);
			methodCount += visitor.getMethodCount();
			locCount += visitor.getLocCount();
		}
		return methodCount == 0 ? 0 : (float) locCount / methodCount;
	}
	
	public float getAvgFieldByClassApp() {
		int fieldCount = 0;
		int classCount = 0;
		for (CompilationUnit unit: units) {
			ClassFieldVisitor visitor = new ClassFieldVisitor();
			unit.accept(visitor);
			fieldCount += visitor.getFieldCount();
			classCount += visitor.getClassCount();
		}
		return classCount == 0 ? 0 : (float) fieldCount / classCount;
	}
	
	public int getNbPackagesApp() {
		PackageVisitor visitor = new PackageVisitor();
		visitAll(visitor);
		return visitor.getPackageCount();
	}
	
	public int getNbMethodsApp() {
		MethodDeclarationVisitor visitor = new MethodDeclarationVisitor();
		visitAll(visitor);
		return visitor.getMethods().size();
	}
	
	public int getNbClassesApp() {
		ClassDeclarationVisitor visitor = new ClassDeclarationVisitor();
		visitAll(visitor);
		return visitor.getClasses().size();
	}
	
	public int getNbLinesApp() {
		final File folder = new File(projectSourcePath);
		ArrayList<File> javaFiles = new ArrayList<>();
		try {
			javaFiles = listJavaFilesForFolder(folder);
		} catch (FolderInvalidException e) {
			e.printStackTrace();
		}
		int lines = 0;
		for (File file : javaFiles) {
			try {
				lines += Files.lines(file.toPath())
				        .filter(s -> !s.trim().isEmpty())
				        .count();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return lines;
	}
	
	public List<String> getTopMethodClassApp(float percent) {
		TopMethodClassVisitor visitor = new TopMethodClassVisitor();
		visitAll(visitor);
		return visitor.getTopPercentClasses(percent);
	}

	public List<String> getTopFieldClassApp(float percent) {
		TopFieldClassVisitor visitor = new TopFieldClassVisitor();
		visitAll(visitor);
		return visitor.getTopPercentClasses(percent);
	}
	
	public List<String> getTopFieldAndMethodClassApp(float percent) {
		List<String> topMethods = getTopMethodClassApp(percent);
		List<String> topFields = getTopFieldClassApp(percent);

	    topMethods.retainAll(topFields);
	    return topMethods;
	}
	
	public List<String> getClassesWithMoreThanXMethods(int x) {
		TopMethodClassVisitor visitor = new TopMethodClassVisitor();
		visitAll(visitor);
		return visitor.getClassesWithMoreThanXMethods(x);
	}
	
	public MapMethodLoc getTopLocMethods(float percent) {
		TopLocMethodVisitor visitor = new TopLocMethodVisitor();
		for (CompilationUnit unit: units) {
			visitor.setCompilationUnit(unit);
			unit.accept(visitor);
		}
		return visitor.getTopPercentMethodsByClass(percent);
	}
	
	public int getMaxParametersApp() {
		MethodDeclarationVisitor visitor = new MethodDeclarationVisitor();
		visitAll(visitor);
		return visitor.getMaxParameters();
	}
	
	private void visitAll(ASTVisitor visitor) {
		for (CompilationUnit unit: units) {
			unit.accept(visitor);
		}
	}
	
	private void createAllParsers() {
		final File folder = new File(projectSourcePath);
		ArrayList<File> javaFiles = new ArrayList<>();
		try {
			javaFiles = listJavaFilesForFolder(folder);
		} catch (FolderInvalidException e) {
			e.printStackTrace();
		}
		
		units = new ArrayList<CompilationUnit>();
		for (File fileEntry: javaFiles) {
			String content;
			try {
				content = FileUtils.readFileToString(fileEntry);
				units.add(parse(content.toCharArray()));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	private CompilationUnit parse(char[] classSource) {
		ASTParser parser = ASTParser.newParser(AST.JLS4);
		configureParser(parser, classSource);
		return (CompilationUnit)parser.createAST(null);
	}
	
	private ArrayList<File> listJavaFilesForFolder(final File folder) throws FolderInvalidException {
		if (folder == null || !folder.isDirectory()) {
			throw new FolderInvalidException();
		}
		ArrayList<File> javaFiles = new ArrayList<File>();
		for (File fileEntry : folder.listFiles()) {
			if (fileEntry.isDirectory()) {
				javaFiles.addAll(listJavaFilesForFolder(fileEntry));
			} else if (fileEntry.getName().contains(".java")) {
				javaFiles.add(fileEntry);
			}
		}

		return javaFiles;
	}
	
	private void configureParser(ASTParser parser, char[] classSource) {
		parser.setResolveBindings(true);
		parser.setKind(ASTParser.K_COMPILATION_UNIT);
		parser.setBindingsRecovery(true);
		Map options = JavaCore.getOptions();
		parser.setCompilerOptions(options);
		parser.setUnitName("");
		
		String[] sources = { projectSourcePath }; 
		String[] classpath = { jrePath };
 
		parser.setEnvironment(classpath, sources, encoding, includeRunningVMBootclasspath);
		parser.setSource(classSource);
	}
}
