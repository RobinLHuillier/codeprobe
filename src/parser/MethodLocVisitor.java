package parser;

import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.CompilationUnit;

public class MethodLocVisitor extends ASTVisitor {
    private CompilationUnit cu;
    private int locCount = 0;
    private int methodCount = 0;

    public MethodLocVisitor(CompilationUnit cu) {
        this.cu = cu;
    }
    
    public boolean visit(MethodDeclaration node) {
        int startLine = cu.getLineNumber(node.getStartPosition());
        int endLine = cu.getLineNumber(node.getStartPosition() + node.getLength());
        locCount += (endLine - startLine + 1);
        methodCount++;
        return super.visit(node);
    }

    public int getLocCount() {
        return locCount;
    }
    
    public int getMethodCount() {
    	return methodCount;
    }
}
