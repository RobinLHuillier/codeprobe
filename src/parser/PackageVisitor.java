package parser;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.PackageDeclaration;

public class PackageVisitor extends ASTVisitor {
    Set<String> packages = new HashSet<>();

    public boolean visit(PackageDeclaration node) {
        packages.add(node.getName().getFullyQualifiedName());
        return super.visit(node);
    }

    public int getPackageCount() {
        return packages.size();
    }
}