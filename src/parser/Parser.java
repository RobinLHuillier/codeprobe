package parser;

import java.util.List;
import models.CallGraph;
import models.MapMethodLoc;


public interface Parser {
	
	public void configure();
	public void initialize();
	
	public String getJrePath();
	public String getProjectPath();
	public void setJrePath(String path);
	public void setProjectPath(String path);
	
	public int getNbClassesApp();
	public int getNbLinesApp();
	public int getNbMethodsApp();
	public int getNbPackagesApp();
	public int getMaxParametersApp();
	public float getAvgMethodByClassApp();
	public float getAvgLocByMethodApp();
	public float getAvgFieldByClassApp();
	public List<String> getTopMethodClassApp(float percent);
	public List<String> getTopFieldClassApp(float percent);
	public List<String> getTopFieldAndMethodClassApp(float percent);
	public List<String> getClassesWithMoreThanXMethods(int min);
	public MapMethodLoc getTopLocMethods(float percent);
	
	public CallGraph getCallGraph();
}
