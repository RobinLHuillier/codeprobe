package parser;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.TypeDeclaration;

public class TopMethodClassVisitor extends ASTVisitor {
    private final Map<String, Integer> classMethodCounts = new HashMap<String, Integer>();

    public boolean visit(TypeDeclaration node) {
        if (!node.isInterface()) {
            String className = node.getName().getFullyQualifiedName();
            int methodCount = node.getMethods().length;
            classMethodCounts.put(className, methodCount);
        }
        return super.visit(node);
    }

    public List<String> getTopPercentClasses(float percent) {
		float sPc = percent < 0 ? 0 : percent > 1 ? 1 : percent;
        int top = (int) Math.ceil(classMethodCounts.size() * sPc);
        return classMethodCounts.entrySet()
                .stream()
                .sorted((e1, e2) -> -e1.getValue().compareTo(e2.getValue()))
                .limit(top)
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }
    
    public List<String> getClassesWithMoreThanXMethods(int x) {
    	return classMethodCounts.entrySet().stream()
                .filter(entry -> entry.getValue() > x)
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }
}
