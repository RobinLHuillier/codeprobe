package parser;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.TypeDeclaration;

import models.MapMethodLoc;
import models.PairMethodLoc;

public class TopLocMethodVisitor extends ASTVisitor {
    private final MapMethodLoc classMethodLocCounts = new MapMethodLoc();
    private CompilationUnit cu;
    
    public boolean visit(TypeDeclaration node) {
        if (!node.isInterface() && cu != null) {
            String className = node.getName().getFullyQualifiedName();
            MethodDeclaration[] methods = node.getMethods();
            List<PairMethodLoc> methodsLoc = new ArrayList<PairMethodLoc>();
            for (MethodDeclaration method: methods) {
            	int startLine = cu.getLineNumber(method.getStartPosition());
                int endLine = cu.getLineNumber(method.getStartPosition() + method.getLength());
                int locCount = (endLine - startLine + 1);
                String methodName = method.getName().getFullyQualifiedName();
                methodsLoc.add(new PairMethodLoc(methodName, locCount));
            }
            classMethodLocCounts.addEntry(className, methodsLoc);
        }
        return super.visit(node);
    }
    
    public void setCompilationUnit(CompilationUnit unit) {
    	cu = unit;
    }
    
    public MapMethodLoc getTopPercentMethodsByClass(float percent) {
    	MapMethodLoc l = new MapMethodLoc();
    	for (String className: classMethodLocCounts.getKeys()) {
    		l.addEntry(className, getTopPercentMethods(className, percent));
    	}
    	return l;
    }

    private List<PairMethodLoc> getTopPercentMethods(String className, float percent) {
		float sPc = percent < 0 ? 0 : percent > 1 ? 1 : percent;
        int top = (int) Math.ceil(classMethodLocCounts.getFromKey(className).size() * sPc);
        return classMethodLocCounts.getFromKey(className)
                .stream()
                .sorted((e1, e2) -> -e1.getLoc().compareTo(e2.getLoc()))
                .limit(top)
                .collect(Collectors.toList());
    }
}
