package models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Graph {
	Map<String, List<String>> vertex;
	
	public Graph() {
		vertex = new HashMap<String, List<String>>();
	}
	
	public List<String> getVertexes() {
		return new ArrayList<>(vertex.keySet());
	}
	
	public List<String> getEdges(String v) {
		return vertex.get(v);
	}
	
	public void addVertex(String v) {
		if (!vertex.containsKey(v)) {
			vertex.put(v, new ArrayList<String>());
		}
	}
	
	public void addEdge(String v, String e) {
		addVertex(v);
		getEdges(v).add(e);
	}
}
