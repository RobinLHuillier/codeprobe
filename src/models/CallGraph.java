package models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.TypeDeclaration;

import exceptions.KeyNotFoundException;
import exceptions.MainNotFoundException;

public class CallGraph {
	Map<MethodDeclaration, List<MethodInvocation>> graph;
	List<String> visited;
	Graph mGraph;
	
	public CallGraph() {
		mGraph = new Graph();
		graph = new HashMap<MethodDeclaration, List<MethodInvocation>>();
	}
	
	public void addMethodDeclaration(MethodDeclaration method) {
		graph.put(method, new ArrayList<MethodInvocation>());
	}
	
	public void addMethodInvocation(MethodDeclaration methodD, MethodInvocation methodI) throws KeyNotFoundException {
		try {
			graph.get(methodD).add(methodI);
		} catch (Exception e) {
			throw new KeyNotFoundException("The method declaration is not a key");
		}
	}
	
	public void addMethodInvocations(MethodDeclaration methodD, List<MethodInvocation> methodI) {
		graph.put(methodD, methodI);
	}
	
	private List<MethodDeclaration> getKeys() {
		return new ArrayList<>(graph.keySet());
	}
	
	private MethodDeclaration getMain() throws MainNotFoundException {
		for (MethodDeclaration m: getKeys()) {
			if (m.getName().getFullyQualifiedName().equals("main")) {
				return m;
			}
		}
		throw new MainNotFoundException();
	}
	
	private String getClassName(MethodDeclaration method) {
		ASTNode parent = method.getParent();
	    while (!(parent instanceof TypeDeclaration)) {
	        parent = parent.getParent();
	    }
	    TypeDeclaration typeDecl = (TypeDeclaration) parent;
	    return typeDecl.getName().getFullyQualifiedName();
	}
	
	private String getClassName(MethodInvocation method) {
        IMethodBinding methodBinding = method.resolveMethodBinding();
        if (methodBinding != null)
        	return methodBinding.getDeclaringClass().getName();
        return null;
	}
	
	
	private String methodToStringWithClass(ASTNode method) {
	    String methodName = "";
	    String className;
	    if (method instanceof MethodDeclaration) {
    	    className = getClassName((MethodDeclaration)method);
    	    methodName = ((MethodDeclaration)method).getName().getFullyQualifiedName();
	    } else if (method instanceof MethodInvocation) {
	    	className = getClassName((MethodInvocation)method);
	    	methodName = ((MethodInvocation)method).getName().getFullyQualifiedName();
	    } else {
	        return "Type non supporté";
	    }
	    
	    if (className == null) {
	    	return null;
	    }

	    return className + "." + methodName;
	}

	private void resetVisited() {
		visited = new ArrayList<String>();
	}
	
	private void addToVisited(String name) {
		visited.add(name);
	}
	
	private MethodDeclaration getDeclarationFromFullName(String fullName) {
		for (MethodDeclaration method: getKeys()) {
			if (methodToStringWithClass(method).equals(fullName)) {
				return method;
			}
		}
		System.out.println(fullName + " : pas trouvé");
		return null;
	}
	
	private String entryToStringRecursive(MethodDeclaration methodD, String tab) {
		if (methodD == null) {
			return "";
		}
		String name =  methodToStringWithClass(methodD);
		mGraph.addVertex(name);
		if (visited.contains(name)) {
			return "";
		}
		addToVisited(name);
		
		StringBuilder s = new StringBuilder();
		
		for (MethodInvocation methodI: graph.get(methodD)) {
			// Don't show the native methods
			if (methodToStringWithClass(methodI) != null) {
				String nameWithClass =  methodToStringWithClass(methodI);
				mGraph.addEdge(name, nameWithClass);
				s.append(tab + "-> " + nameWithClass + "\n");
				MethodDeclaration m = getDeclarationFromFullName(nameWithClass);
				s.append(entryToStringRecursive(m, tab+"  "));
			}
		}
		return s.toString();
	}
	
	public String toString() {
		StringBuilder s = new StringBuilder();
		
		MethodDeclaration main;
		try {
			main = getMain();
		} catch (MainNotFoundException m) {
			return "Main not found.";
		}
		
		s.append(methodToStringWithClass(main) + " : \n");
		resetVisited();
		s.append(entryToStringRecursive(main, "  "));
		
		return s.toString();
	}
	
	public Graph getGraph() {
		mGraph = new Graph();
		this.toString();
		return mGraph;
	}
}
