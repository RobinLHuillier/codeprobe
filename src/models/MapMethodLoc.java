package models;

import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

public class MapMethodLoc {
	Map<String, List<PairMethodLoc>> map;
	
	public MapMethodLoc() {
		map = new HashMap<String, List<PairMethodLoc>>();
	}
	
	public void addEntry(String className, List<PairMethodLoc> methodsLoc) {
		map.put(className, methodsLoc);
	}
	
	public List<String> getKeys() {
		return new ArrayList<>(map.keySet());
	}
	
	public List<PairMethodLoc> getFromKey(String className) {
		return map.get(className);
	}
}
