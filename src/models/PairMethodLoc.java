package models;

public class PairMethodLoc {
	private String methodName;
	private Integer loc;

	public PairMethodLoc() {}
	
	public PairMethodLoc(String methodName, Integer loc) {
		this.setMethodName(methodName);
		this.setLoc(loc);
	}
	
	public String getMethodName() {
		return methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	public Integer getLoc() {
		return loc;
	}

	public void setLoc(Integer loc) {
		this.loc = loc;
	}
}
