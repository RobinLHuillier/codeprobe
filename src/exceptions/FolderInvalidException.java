package exceptions;

public class FolderInvalidException extends Exception {

	public FolderInvalidException() {}
	
	public FolderInvalidException(String message) {
		super(message);
	}
}