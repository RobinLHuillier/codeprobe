package exceptions;

public class KeyNotFoundException extends Exception {

	public KeyNotFoundException() {}
	
	public KeyNotFoundException(String message) {
		super(message);
	}
}