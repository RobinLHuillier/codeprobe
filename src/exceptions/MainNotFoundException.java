package exceptions;

public class MainNotFoundException extends Exception {

	public MainNotFoundException() {}
	
	public MainNotFoundException(String message) {
		super(message);
	}
}