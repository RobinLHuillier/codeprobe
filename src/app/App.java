package app;

import parser.JDTASTParser;
import ui.CommandLine;
import ui.GUI;

public class App {
	public static void main(String[] args) {
		String projectPath = ".";
		String jrePath = System.getProperty("java.home");

		// Parser parser = new JDTASTParser(projectPath, jrePath);
		// pour voir un graphe d'appel plus intéressant à partir du main
		JDTASTParser parser = new JDTASTParser(projectPath, jrePath);
		parser.configure();
		parser.initialize();
		
		for (String arg : args) {
		    if ("-cli".equals(arg)) {
		    	CommandLine cli = new CommandLine();
				cli.start(parser);
		    } else if ("-gui".equals(arg)) {
		    	GUI gui = new GUI();
				gui.start(parser);
		    }
		}
	}
}
