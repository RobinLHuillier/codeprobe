package ui;

import java.util.InputMismatchException;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

import models.CallGraph;
import models.MapMethodLoc;
import models.PairMethodLoc;
import parser.Parser;

public class CommandLine implements UI {
	private Parser parser;
	private Scanner scanner;
	
	public CommandLine() {}
	
	public void start(Parser parser) {
		this.parser = parser;
		scanner = new Scanner(System.in);
		scanner.useLocale(Locale.US); // to write a float 0.1 and not 0,1
		
		displayTopInformation();
		
		scanner.close();
	}
	
	private void getEntryMenu() {
		int choix = -1;
		
		while (choix < 0 || choix > 7) {
			System.out.print("Entrez votre choix : ");
			try {
				choix = scanner.nextInt();
			} catch (InputMismatchException e) {
				System.out.println("Veuillez taper un entier.");
				scanner.next();
			}
		}
		
		switch (choix) {
			case 1:
				changeProjectPath();
				break;
			case 2:
				getTopMethod();
				break;
			case 3:
				getTopField();
				break;
			case 4:
				getTopFieldAndMethod();
				break;
			case 5:
				getClassesWithMoreThanXMethods();
				break;
			case 6:
				getTopLocMethods();
				break;
			case 7:
				getCallGraph();
				break;
			default:
			case 0:
				System.out.println("Au revoir");
				break;
		}

		if (choix > 1) {
			displayMenu();
		}
	}
	
	private void getTopField() {
		System.out.println("Entrez le pourcentage (entre 0 et 1).");
		float pc = getEntryFloat();
		List<String> names = parser.getTopFieldClassApp(pc);
		System.out.println("Top des classes qui possèdent le plus grand nombre d'attributs : " + String.join(" - ", names));
	}
	
	private void getTopMethod() {
		System.out.println("Entrez le pourcentage (entre 0 et 1).");
		float pc = getEntryFloat();
		List<String> names = parser.getTopMethodClassApp(pc);
		System.out.println("Top des classes qui possèdent le plus grand nombre de méthodes : " + String.join(" - ", names));
	}
	
	private void getTopFieldAndMethod() {
		System.out.println("Entrez le pourcentage (entre 0 et 1).");
		float pc = getEntryFloat();
		List<String> names = parser.getTopFieldAndMethodClassApp(pc);
		System.out.println("Classes qui sont dans les deux tops : " + String.join(" - ", names));
	}
	
	private void getClassesWithMoreThanXMethods() {
		System.out.println("Entrez le nombre de méthodes.");
		int count = getEntryInt();
		List<String> names = parser.getClassesWithMoreThanXMethods(count);
		System.out.println("Classes qui ont plus de " + count + " méthodes : " + String.join(" - ", names));
	}
	
	private void getTopLocMethods() {
		System.out.println("Entrez le pourcentage (entre 0 et 1).");
		float pc = getEntryFloat();
		MapMethodLoc topLocMethods = parser.getTopLocMethods(pc);
		System.out.println("Top des méthodes avec le plus de lignes de code par classe : ");
		for (String className: topLocMethods.getKeys()) {
			StringBuilder s = new StringBuilder();
			s.append(className + " : ");
			for (PairMethodLoc method: topLocMethods.getFromKey(className)) {
				s.append("( " + method.getMethodName() + " - " + method.getLoc() + "loc )");
			}
			System.out.println(s.toString());
		}
	}
	
	private void getCallGraph() {
		CallGraph graph = parser.getCallGraph();
		System.out.println(graph.toString());
	}
	
	private void changeProjectPath() {
		System.out.println("Entrez le path du projet à analyser.");
		String path = getEntryString();
		parser.setProjectPath(path);
		parser.initialize();
		displayTopInformation();
	}
	
	private String getEntryString() {
		System.out.print("Chaine de caractères : ");
		String s = "";
		while (s.length() < 1) {
			s = scanner.nextLine();
		}
		return s;
	}
	
	private int getEntryInt() {
		int value = 0;
		boolean valid = false;
		while (!valid) {
			System.out.print("Entier : ");
			try {
				value = scanner.nextInt();
				valid = true;
			} catch (InputMismatchException e) {
				System.out.println("Un entier est attendu");
				scanner.next();
			}
		}
		return value;
	}
	
	private float getEntryFloat() {
		float value = 0;
		boolean valid = false;
		while (!valid) {
			System.out.print("Flottant : ");
			try {
				value = scanner.nextFloat();
				valid = true;
			} catch (InputMismatchException e) {
				System.out.println("Un flottant est attendu");
				scanner.next();
			}
		}
		return value;
	}
	
	private void displayBasicInformations() {
		int nb = parser.getNbClassesApp();
		System.out.println("Nombre de classes de l'application : " + nb);
		nb = parser.getNbLinesApp();
		System.out.println("Nombres de LoC de l'application : " + nb);
		nb = parser.getNbMethodsApp();
		System.out.println("Nombres de methodes de l'application : " + nb);
		nb = parser.getNbPackagesApp();
		System.out.println("Nombres de packages de l'application : " + nb);
		float lg = parser.getAvgMethodByClassApp();
		System.out.println("Nombre moyen de méthodes par classe : " + lg);
		lg = parser.getAvgLocByMethodApp();
		System.out.println("Nombre moyen de LoC par méthode : " + lg);
		lg = parser.getAvgFieldByClassApp();
		System.out.println("Nombre moyen d'attributs par classe : " + lg);
	}
	
	private void displayTopInformation() {
		System.out.println("CodeProbe - Analyse de code source");
		System.out.println("----------------------------------");
		System.out.println("Path projet : " + parser.getProjectPath());
		System.out.println("----------------------------------");
		displayBasicInformations();
		displayMenu();
	}
	
	private void displayMenu() {
		System.out.println("----------------------------------");
		System.out.println("1 - Changer de projet");
		System.out.println("2 - Top X % des classes qui possèdent le plus grand nombre de méthodes");
		System.out.println("3 - Top X % des classes qui possèdent le plus grand nombre d'attributs");
		System.out.println("4 - Classes qui sont dans les deux top X %");
		System.out.println("5 - Classes qui ont plus de X méthodes");
		System.out.println("6 - X % des méthodes avec le plus de lignes de code par classe");
		System.out.println("7 - Afficher le graphe d'appel");
		System.out.println("0 - Quitter");
		getEntryMenu();
	}
}
