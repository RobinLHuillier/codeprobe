package ui;

import parser.Parser;

public interface UI {
	public void start(Parser parser);
}
