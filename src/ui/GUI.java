package ui;

import java.awt.Font;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import com.mxgraph.layout.hierarchical.mxHierarchicalLayout;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.view.mxGraph;

import models.Graph;
import models.MapMethodLoc;
import models.PairMethodLoc;
import parser.Parser;

public class GUI implements UI {
	private Parser parser;
	private String projectPath;
	
	private JFrame frame;
	private JPanel page;
	
	public void start(Parser parser) {
		this.parser = parser;
		setUp();
	}
	
	private void resetPage() {
        this.page = new JPanel();
        this.page.setSize(1000, 500);
        this.page.setLayout(null);
        this.frame.setContentPane(page);
    }
	
	private boolean selectProjectPath() {
		JFileChooser fileChooser = new JFileChooser(new File("."));
	    fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
	    int returnValue = fileChooser.showOpenDialog(null);
	    
	    if (returnValue == JFileChooser.APPROVE_OPTION) {
	        File selectedFile = fileChooser.getSelectedFile();
	        projectPath = selectedFile.getAbsolutePath();
	        parser.setProjectPath(projectPath);
	        parser.initialize();
	        return true;
	    }
	    
	    return false;
	}
	
	private void creerSelectionProjet() {
		resetPage();
		
		JLabel header = new JLabel("Application d'analyse statique de code C O D E P R O B E ", JLabel.CENTER);
        header.setBounds(0, 50, 1000, 30);
        header.setFont(new Font("Serif", Font.BOLD, 20));
        this.page.add(header);
        
        JButton bSelect = new JButton("Sélectionner le projet à analyser");
        bSelect.setBounds(350, 150, 300, 70);
        bSelect.addActionListener(e -> {
            if (this.selectProjectPath()) {
            	this.creerInformationsPrincipales();
            	this.addBoutonsInfosPrincipales();
            }
        });
        this.page.add(bSelect);
	}
	
	private void addReturnInfosPrincipales() {
		JButton bReturn = new JButton("Options précédentes");
		bReturn.setBounds(50, 400, 400, 40);
		bReturn.addActionListener(e -> {
            this.creerInformationsPrincipales();
            this.addBoutonsInfosPrincipales();
        });
        this.page.add(bReturn);
	}
	
	private void displayTopMethod() {
	    displayTopMethod(0.1f);
	}
	
	private void displayTopMethod(float percent) {
		this.creerInformationsPrincipales();
		
		JLabel lInt = new JLabel("Pourcentage (0-1) [presser entrée]");
        lInt.setFont(new Font("Serif", Font.BOLD, 15));
		lInt.setBounds(550, 15, 400, 40);
        this.page.add(lInt);
        
        JTextField tInt = new JTextField(String.valueOf(percent));
        tInt.setBounds(550, 50, 400, 30);
        tInt.addActionListener(e -> {
            JTextField textField = (JTextField)e.getSource();
           	float entry = Float.valueOf(textField.getText());
           	displayTopMethod(entry);
        });
        this.page.add(tInt);
        
        JLabel lTop = new JLabel("Top des classes qui possèdent le plus grand nombre de méthodes : ", JLabel.LEFT);
        lTop.setBounds(510, 90, 480, 30);
        lTop.setFont(new Font("Serif", Font.BOLD, 15));
        this.page.add(lTop);

		List<String> names = parser.getTopMethodClassApp(percent);
		JPanel contentPanel = new JPanel();
        contentPanel.setLayout(new BoxLayout(contentPanel, BoxLayout.Y_AXIS));
        
        for (int i=0; i<names.size(); i++) {
        	JLabel lName = new JLabel(names.get(i), JLabel.LEFT);
        	lName.setFont(new Font("Serif", Font.BOLD, 15));
        	contentPanel.add(lName);
        }

        JScrollPane scrollPane = new JScrollPane(contentPanel);
        scrollPane.setBounds(520, 130, 460, 330);
        this.page.add(scrollPane);
		
		this.addReturnInfosPrincipales();
		
		this.page.revalidate();
		this.page.repaint();
	}
	
	private void displayTopField() {
	    displayTopField(0.1f);
	}
	
	private void displayTopField(float percent) {
		this.creerInformationsPrincipales();
		
		JLabel lInt = new JLabel("Pourcentage (0-1) [presser entrée]");
        lInt.setFont(new Font("Serif", Font.BOLD, 15));
		lInt.setBounds(550, 15, 400, 40);
        this.page.add(lInt);
        
        JTextField tInt = new JTextField(String.valueOf(percent));
        tInt.setBounds(550, 50, 400, 30);
        tInt.addActionListener(e -> {
            JTextField textField = (JTextField)e.getSource();
           	float entry = Float.valueOf(textField.getText());
           	displayTopField(entry);
        });
        this.page.add(tInt);
        
        JLabel lTop = new JLabel("Top des classes qui possèdent le plus grand nombre d'attributs : ", JLabel.LEFT);
        lTop.setBounds(510, 90, 480, 30);
        lTop.setFont(new Font("Serif", Font.BOLD, 15));
        this.page.add(lTop);

		List<String> names = parser.getTopFieldClassApp(percent);
		JPanel contentPanel = new JPanel();
        contentPanel.setLayout(new BoxLayout(contentPanel, BoxLayout.Y_AXIS));
        
        for (int i=0; i<names.size(); i++) {
        	JLabel lName = new JLabel(names.get(i), JLabel.LEFT);
        	lName.setFont(new Font("Serif", Font.BOLD, 15));
        	contentPanel.add(lName);
        }
		
        JScrollPane scrollPane = new JScrollPane(contentPanel);
        scrollPane.setBounds(520, 130, 460, 330);
        this.page.add(scrollPane);
        
		this.addReturnInfosPrincipales();
		
		this.page.revalidate();
		this.page.repaint();
	}
	
	private void displayBothTop() {
		this.displayBothTop(0.1f);
	}
	
	private void displayBothTop(float percent) {
		this.creerInformationsPrincipales();
		
		JLabel lInt = new JLabel("Pourcentage (0-1) [presser entrée]");
        lInt.setFont(new Font("Serif", Font.BOLD, 15));
		lInt.setBounds(550, 15, 400, 40);
        this.page.add(lInt);
        
        JTextField tInt = new JTextField(String.valueOf(percent));
        tInt.setBounds(550, 50, 400, 30);
        tInt.addActionListener(e -> {
            JTextField textField = (JTextField)e.getSource();
           	float entry = Float.valueOf(textField.getText());
           	displayBothTop(entry);
        });
        this.page.add(tInt);
        
        JLabel lTop = new JLabel("Top des classes qui possèdent le plus d'attributs et de méthodes : ", JLabel.LEFT);
        lTop.setBounds(510, 90, 480, 30);
        lTop.setFont(new Font("Serif", Font.BOLD, 15));
        this.page.add(lTop);

		List<String> names = parser.getTopFieldAndMethodClassApp(percent);
		JPanel contentPanel = new JPanel();
        contentPanel.setLayout(new BoxLayout(contentPanel, BoxLayout.Y_AXIS));
        
        for (int i=0; i<names.size(); i++) {
        	JLabel lName = new JLabel(names.get(i), JLabel.LEFT);
        	lName.setFont(new Font("Serif", Font.BOLD, 15));
        	contentPanel.add(lName);
        }

        JScrollPane scrollPane = new JScrollPane(contentPanel);
        scrollPane.setBounds(520, 130, 460, 330);
        this.page.add(scrollPane);
        
		this.addReturnInfosPrincipales();
		
		this.page.revalidate();
		this.page.repaint();
	}
	
	private void displayMoreThanXMethods() {
		this.displayMoreThanXMethods(20);
	}
	
	private void displayMoreThanXMethods(int count) {
		this.creerInformationsPrincipales();
		
		JLabel lInt = new JLabel("Entier [presser entrée]");
        lInt.setFont(new Font("Serif", Font.BOLD, 15));
		lInt.setBounds(550, 15, 400, 40);
        this.page.add(lInt);
        
        JTextField tInt = new JTextField(String.valueOf(count));
        tInt.setBounds(550, 50, 400, 30);
        tInt.addActionListener(e -> {
            JTextField textField = (JTextField)e.getSource();
           	int entry = Integer.valueOf(textField.getText());
           	displayMoreThanXMethods(entry);
        });
        this.page.add(tInt);
        
        JLabel lTop = new JLabel("Classes qui ont plus de " + count + " méthodes : ", JLabel.LEFT);
        lTop.setBounds(510, 90, 480, 30);
        lTop.setFont(new Font("Serif", Font.BOLD, 15));
        this.page.add(lTop);

		List<String> names = parser.getClassesWithMoreThanXMethods(count);
		JPanel contentPanel = new JPanel();
        contentPanel.setLayout(new BoxLayout(contentPanel, BoxLayout.Y_AXIS));
        
        for (int i=0; i<names.size(); i++) {
        	JLabel lName = new JLabel(names.get(i), JLabel.LEFT);
        	lName.setFont(new Font("Serif", Font.BOLD, 15));
        	contentPanel.add(lName);
        }
        
        JScrollPane scrollPane = new JScrollPane(contentPanel);
        scrollPane.setBounds(520, 130, 460, 330);
        this.page.add(scrollPane);
        
		this.addReturnInfosPrincipales();
		
		this.page.revalidate();
		this.page.repaint();
	}
	
	private void displayTopLocMethod() {
		this.displayTopLocMethod(0.1f);
	}
	
	private void displayTopLocMethod(float percent) {
		this.creerInformationsPrincipales();
		
		JLabel lInt = new JLabel("Pourcentage (0-1) [presser entrée]");
        lInt.setFont(new Font("Serif", Font.BOLD, 15));
		lInt.setBounds(550, 15, 400, 40);
        this.page.add(lInt);
        
        JTextField tInt = new JTextField(String.valueOf(percent));
        tInt.setBounds(550, 50, 400, 30);
        tInt.addActionListener(e -> {
            JTextField textField = (JTextField)e.getSource();
           	float entry = Float.valueOf(textField.getText());
           	displayTopLocMethod(entry);
        });
        this.page.add(tInt);
        
        JLabel lTop = new JLabel("Top des méthodes avec le plus de lignes de code par classe : ", JLabel.LEFT);
        lTop.setBounds(510, 90, 480, 30);
        lTop.setFont(new Font("Serif", Font.BOLD, 15));
        this.page.add(lTop);

        MapMethodLoc topLocMethods = parser.getTopLocMethods(percent);
        JPanel contentPanel = new JPanel();
        contentPanel.setLayout(new BoxLayout(contentPanel, BoxLayout.Y_AXIS));
        
        for (int i=0; i<topLocMethods.getKeys().size(); i++) {
        	String className = topLocMethods.getKeys().get(i);
        	StringBuilder s = new StringBuilder();
			s.append(className + " : ");
			for (PairMethodLoc method: topLocMethods.getFromKey(className)) {
				s.append("(" + method.getMethodName() + "-" + method.getLoc() + " loc)");
			}
        	JLabel lName = new JLabel(s.toString(), JLabel.LEFT);
        	lName.setFont(new Font("Serif", Font.BOLD, 15));
        	contentPanel.add(lName);
        }
        
        JScrollPane scrollPane = new JScrollPane(contentPanel);
        scrollPane.setBounds(520, 130, 460, 330);
        this.page.add(scrollPane);
		
		this.addReturnInfosPrincipales();
		
		this.page.revalidate();
		this.page.repaint();
	}
	
	private void displayCallGraph() {		
		this.resetPage();
		
		Graph mGraph = parser.getCallGraph().getGraph();
		mxGraph graph = new mxGraph();
		Object parent = graph.getDefaultParent();
		graph.getModel().beginUpdate();
		
		Map<String, Object> vertexes = new HashMap<String, Object>();
		for (String vertex: mGraph.getVertexes()) {
			Object v = graph.insertVertex(parent, null, vertex, 0, 0, vertex.length()*7, 30);
			vertexes.put(vertex, v);
		}
		for (String vertex: mGraph.getVertexes()) {
			Object v1 = vertexes.get(vertex);
			for (String edge: mGraph.getEdges(vertex)) {
				Object v2 = vertexes.get(edge);
				graph.insertEdge(parent, null, "", v1, v2);
			}
		}
		
		graph.getModel().endUpdate();

		mxHierarchicalLayout layout = new mxHierarchicalLayout(graph);
		layout.execute(graph.getDefaultParent());
		mxGraphComponent graphComponent = new mxGraphComponent(graph);
		graphComponent.setBounds(20, 20, 960, 370);
		
		this.page.add(graphComponent);

		this.addReturnInfosPrincipales();
		
		this.page.revalidate();
		this.page.repaint();
	}
	
	private void addBoutonsInfosPrincipales() {
		JButton bChange = new JButton("Changer de projet");
		bChange.setBounds(550, 100, 400, 40);
		bChange.addActionListener(e -> {
            if (this.selectProjectPath()) {
            	this.creerInformationsPrincipales();
            	this.addBoutonsInfosPrincipales();
            }
        });
        this.page.add(bChange);
		
        JButton bTopMethod = new JButton("Top X % des classes [Methodes]");
        bTopMethod.setBounds(550, 150, 400, 40);
        bTopMethod.addActionListener(e -> {
            this.displayTopMethod();
        });
        this.page.add(bTopMethod);
        
        JButton bTopField = new JButton("Top X % des classes [Attributs]");
        bTopField.setBounds(550, 200, 400, 40);
        bTopField.addActionListener(e -> {
            this.displayTopField();
        });
        this.page.add(bTopField);
        
        JButton bBothTop = new JButton("Top X % des classes [Attributs + Methodes]");
        bBothTop.setBounds(550, 250, 400, 40);
        bBothTop.addActionListener(e -> {
            this.displayBothTop();
        });
        this.page.add(bBothTop);
        
        JButton bMoreThan = new JButton("Classes qui ont plus de X méthodes");
        bMoreThan.setBounds(550, 300, 400, 40);
        bMoreThan.addActionListener(e -> {
            this.displayMoreThanXMethods();
        });
        this.page.add(bMoreThan);
        
        JButton bTopLoc = new JButton("X % des méthodes avec le plus de lignes de code par classe");
        bTopLoc.setBounds(550, 350, 400, 40);
        bTopLoc.addActionListener(e -> {
            this.displayTopLocMethod();
        });
        this.page.add(bTopLoc);
        
        JButton bCallGraph = new JButton("Afficher le graphe d'appel");
        bCallGraph.setBounds(550, 400, 400, 40);
        bCallGraph.addActionListener(e -> {
            this.displayCallGraph();
        });
        this.page.add(bCallGraph);
	}
	
	private void creerInformationsPrincipales() {
		resetPage();
		
        JLabel lPath = new JLabel("Projet : " + projectPath, JLabel.LEFT);
        lPath.setBounds(30, 25, 470, 30);
        lPath.setFont(new Font("Serif", Font.BOLD, 15));
        this.page.add(lPath);
        
        int nb = parser.getNbClassesApp();
        JLabel l1 = new JLabel("Nombre de classes de l'application : " + nb, JLabel.LEFT);
        l1.setBounds(30, 100, 470, 30);
        l1.setFont(new Font("Serif", Font.BOLD, 15));
        this.page.add(l1);
        
        nb = parser.getNbLinesApp();
        JLabel l2 = new JLabel("Nombres de LoC de l'application : " + nb, JLabel.LEFT);
        l2.setBounds(30, 125, 470, 30);
        l2.setFont(new Font("Serif", Font.BOLD, 15));
        this.page.add(l2);
        
		nb = parser.getNbMethodsApp();
        JLabel l3 = new JLabel("Nombres de methodes de l'application : " + nb, JLabel.LEFT);
        l3.setBounds(30, 150, 470, 30);
        l3.setFont(new Font("Serif", Font.BOLD, 15));
        this.page.add(l3);
		
		nb = parser.getNbPackagesApp();
        JLabel l4 = new JLabel("Nombres de packages de l'application : " + nb, JLabel.LEFT);
        l4.setBounds(30, 175, 470, 30);
        l4.setFont(new Font("Serif", Font.BOLD, 15));
        this.page.add(l4);
		
		float lg = parser.getAvgMethodByClassApp();
        JLabel l5 = new JLabel("Nombre moyen de méthodes par classe : " + lg, JLabel.LEFT);
        l5.setBounds(30, 200, 470, 30);
        l5.setFont(new Font("Serif", Font.BOLD, 15));
        this.page.add(l5);
		
		lg = parser.getAvgLocByMethodApp();
        JLabel l6 = new JLabel("Nombre moyen de LoC par méthode : " + lg, JLabel.LEFT);
        l6.setBounds(30, 225, 470, 30);
        l6.setFont(new Font("Serif", Font.BOLD, 15));
        this.page.add(l6);
		
		lg = parser.getAvgFieldByClassApp();
        JLabel l7 = new JLabel("Nombre moyen d'attributs par classe : " + lg, JLabel.LEFT);
        l7.setBounds(30, 250, 470, 30);
        l7.setFont(new Font("Serif", Font.BOLD, 15));
        this.page.add(l7);
	}
	
	private void setUp() {
		this.frame = new JFrame("CodeProbe");
        this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.frame.setSize(1000, 500);
        this.creerSelectionProjet();
        this.frame.setVisible(true);
	}
}
